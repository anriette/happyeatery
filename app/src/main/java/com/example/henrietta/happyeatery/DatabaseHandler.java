package com.example.henrietta.happyeatery;

public interface DatabaseHandler {
    void getDBstatus();
    void addToFavourites();
    void addToDatabase();

    void removeFromFavourites();
}
