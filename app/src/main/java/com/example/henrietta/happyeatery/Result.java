package com.example.henrietta.happyeatery;

import android.app.SearchManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

public class Result extends FragmentActivity implements OnMapReadyCallback, DatabaseHandler {

    LatLng mLocation;
    int mDistance;
    int mRating;
    TextView mHeader;
    TextView mInformation;
    Switch mFavourite;
    LinearLayout mInfoLayout;
    LinearLayout mButtons;
    Button mLookUp;
    Button mDirections;
    int mRestaurant;    // holds FSAid of selected restaurant
    String mCity;

    private Marker myMarker;
    private GoogleMap mMap;
    private static final String MY_LOCATION = "My location";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.map_restaurant_portrait);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.uiResultsMapFragment);
        mapFragment.getMapAsync(this);

        Bundle position = getIntent().getParcelableExtra("bundle");
        String enteredLocation = position.getString("enteredLocation");
        mLocation = position.getParcelable("location");
        mDistance = position.getInt("distance", 1);
        mRating = position.getInt("rating", 5);

        mHeader = findViewById(R.id.uiRestaurantHeaderTextView);
        mHeader.setText(this.getString(R.string.resultsHeader) + " " + enteredLocation);

        mInformation = findViewById(R.id.uiRestaurantInfoTextTextView);
        mFavourite = findViewById(R.id.uiFavouriteSwitch);
        mInfoLayout = findViewById(R.id.uiInfoTextLinearLayout);
        mButtons = findViewById(R.id.uiButtonsLinearLayout);
        mFavourite.setVisibility(View.INVISIBLE);
        mInfoLayout.setVisibility(View.INVISIBLE);
        mButtons.setVisibility(View.INVISIBLE);
        mLookUp = findViewById(R.id.uiLookUpButton);
        mDirections = findViewById(R.id.uiDirectionsButton);

        mFavourite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {    // updates whether the Restaurant is in the Favourites database, based on switch status
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked == true)
                    addToFavourites();
                else
                    removeFromFavourites();
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng resultPosition = mLocation;

        mMap.addMarker(new MarkerOptions().position(resultPosition).icon(BitmapDescriptorFactory.defaultMarker(0)).title(MY_LOCATION));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(resultPosition, 15));

        mMap.setOnMarkerClickListener((new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                int restaurant = (int)(marker.getTag());

                updateInfo(restaurant); // gets info on Restaurant and displays it
                getDBstatus();          // adjusts switch according to DB status

                return false;
            }
        }));

        getRestaurants();   // gets restaurants and shows them on the map, once the map has loaded
    }

    @Override
    protected void onPause() {
        super.onPause();

        setSharedPreferences();
    }

    private void setSharedPreferences() {   //stores the entered information in case of onPause/onStop
        SharedPreferences resultPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor resultEditor = resultPreferences.edit();
        resultEditor.putString("latitude", mLocation.latitude + "");
        resultEditor.putString("longitude", mLocation.longitude + "");

        resultEditor.putInt("distance", mDistance);
        resultEditor.putInt("rating", mRating);
        resultEditor.putInt("FSAid", mRestaurant);

        resultEditor.apply();
    }

    @Override
    protected void onResume() {
        super.onResume();

        getSharedPreferences();
    }

    private void getSharedPreferences() {   // gets stored information in case of onPause/onStop
        SharedPreferences resultPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        if (mRestaurant != 0) { //updates search terms when a new search is made (aka, no establishment has yet been selected)
            double latitude = Double.parseDouble(resultPreferences.getString("latitude", ""));
            double longitude = Double.parseDouble(resultPreferences.getString("longitude", ""));
            mLocation = new LatLng(latitude, longitude);

            mDistance = resultPreferences.getInt("distance", 0);
            mRating = resultPreferences.getInt("rating", 0);
            mRestaurant = resultPreferences.getInt("FSAid", 0);
        }
    }

    private void updateInfo(final int restaurant) {
        String requestUrl = "http://api.ratings.food.gov.uk/Establishments?" +
                "id=" + restaurant +
                "&pageSize=50&pageNumber=1";

        RequestQueue queue = Volley.newRequestQueue(this);

        JsonObjectRequest jsonObjRequest = new JsonObjectRequest(Request.Method.GET, requestUrl , null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String businessName = response.getString("BusinessName");
                            int rating = response.getInt("RatingValue");
                            JSONObject scores = response.getJSONObject("scores");
                            int hygiene = calculateDisplayRating(scores.getInt("Hygiene"));
                            int cleanliness = calculateDisplayRating(scores.getInt("Structural"));
                            int management = calculateDisplayRating(scores.getInt("ConfidenceInManagement"));
                            String inspectionDate = response.getString("RatingDate");
                            String businessType = response.getString("BusinessType");
                            String postCode = response.getString("PostCode");
                            String address = response.getString("AddressLine1");

                            //displays data
                            String date = inspectionDate.substring(0,10);   //only prints date, not time
                            mInformation.setText(rating + " / 5\n" +
                                        hygiene + "\n" +
                                        cleanliness + "\n" +
                                        management + "\n" +
                                        date + "\n" +
                                        businessType + "\n" +
                                        address + " " + postCode);

                            //sets info for buttons
                            String[] addressParts = address.split(" ");         // divides address field into several parts
                            String lastPart = addressParts[addressParts.length-1];    // extracts city
                            mCity = lastPart;

                            //updates GUI
                            mHeader.setText(businessName);  // updates to visible when an establishment has been selected
                            mFavourite.setVisibility(View.VISIBLE);
                            mInfoLayout.setVisibility(View.VISIBLE);
                            mButtons.setVisibility(View.VISIBLE);

                            mRestaurant = restaurant;

                        } catch (JSONException e) {
                            e.printStackTrace();
                            mHeader.setText("No information available");
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mHeader.setText("Cannot connect to server. Try again!");
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String>  headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                headers.put("x-api-version", "2");
                return headers;
            }
        };
        queue.add(jsonObjRequest);
    }

    public int calculateDisplayRating(int DBRating) {   //changes displayed number to one that makes sense for the user
        int displayResult;

        if (DBRating == 0)
            displayResult = 5;
        else if (DBRating == 5)
            displayResult = 4;
        else if (DBRating == 10)
            displayResult = 3;
        else if (DBRating == 15)
            displayResult = 2;
        else if (DBRating == 20)
            displayResult = 1;
        else
            displayResult = 0;

        return displayResult;
    }

    public void getDBstatus() {
        FavouritesDatabase faveDB = new FavouritesDatabase(getApplicationContext());
        Favourite check = faveDB.getFavourite(mRestaurant);
        if (check != null)
            mFavourite.setChecked(true);
        else
            mFavourite.setChecked(false);
        faveDB.close();
    }

    //onClick for look up button - to look up restaurant opening hours
    public void lookUpOnline(View view) {
        String restaurant = mHeader.getText().toString();

        Intent openingHoursIntent = new Intent (Intent.ACTION_WEB_SEARCH);
        openingHoursIntent.putExtra(SearchManager.QUERY, restaurant + " " + mCity + " opening hours");
        startActivity(openingHoursIntent);
    }

    //onClick for share button - to share restaurant via app chosen by user
    public void shareRestaurant(View view) {
        String restaurant = mHeader.getText().toString();

        Intent shareIntent = new Intent (Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, "Check out " + restaurant + " in " + mCity + "! I think you might like it.");
        startActivity(shareIntent);
    }

    public void getRestaurants() {

        String requestUrl = "http://api.ratings.food.gov.uk/Establishments?" +
                            "longitude=" + mLocation.longitude +
                            "&latitude=" + mLocation.latitude  +
                            "&maxDistanceLimit=" + mDistance  +
                            "&schemeTypeKey=FHRS&&ratingKey=" + mRating + //"||&schemeTypeKey=FHIS&&ratingKey=pass" // Note that this does not work, but something like it is needed to integrate Scotland into the system
                            "&sortOptionKey=Distance" +
                            "&pageSize=50&pageNumber=1";// + "&ratingOperatorKey=GreaterThanOrEqual";

        RequestQueue queue = Volley.newRequestQueue(this);

        JsonObjectRequest jsonObjRequest = new JsonObjectRequest(Request.Method.GET, requestUrl , null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray restaurants = response.getJSONArray("establishments");

                            for (int i = 0; i < restaurants.length(); i++) {
                                JSONObject restaurant = restaurants.getJSONObject(i);

                                //Info for Map
                                String businessName = restaurant.getString("BusinessName");
                                JSONObject geoCode = restaurant.getJSONObject("geocode");
                                double restaurantLongitude = geoCode.getDouble("longitude");
                                double restaurantLatitude = geoCode.getDouble("latitude");
                                double distanceFromStartPosition = restaurant.getDouble("Distance");
                                DecimalFormat df = new DecimalFormat("#.##");
                                String distance = df.format(distanceFromStartPosition);

                                LatLng restaurantPosition = new LatLng(restaurantLatitude, restaurantLongitude);
                                myMarker = mMap.addMarker(new MarkerOptions().position(restaurantPosition).icon(BitmapDescriptorFactory.defaultMarker(205)).title(distance + " miles away"));

                                int restaurantID = restaurant.getInt("FHRSID");
                                myMarker.setTag(restaurantID);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            mHeader.setText("Nothing found");
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mHeader.setText("Cannot connect to server. Try again!");
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String>  headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                headers.put("x-api-version", "2");
                return headers;
            }
        };
        queue.add(jsonObjRequest);
    }

    public void addToFavourites() { //checks if restaurant is in faveDB, adds if it is not.
        FavouritesDatabase faveDB = new FavouritesDatabase(this);
        Favourite check = faveDB.getFavourite(mRestaurant);

        if(check == null) {
            addToDatabase();
        }

        faveDB.close();
    }

    public void removeFromFavourites() { //checks if restaurant is in faveDB, removes if it is.
        FavouritesDatabase faveDB = new FavouritesDatabase(this);
        Favourite check = faveDB.getFavourite(mRestaurant);
        if(check != null)
            faveDB.deleteFavourite(mRestaurant);
        faveDB.close();
    }

    public void addToDatabase() {
        String requestUrl = "http://api.ratings.food.gov.uk/Establishments?" +
                "id=" + mRestaurant +
                "&pageSize=50&pageNumber=1";

        RequestQueue queue = Volley.newRequestQueue(this);

        JsonObjectRequest jsonObjRequest = new JsonObjectRequest(Request.Method.GET, requestUrl , null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            //Info for database
                            String businessName = response.getString("BusinessName");
                            JSONObject geocode = response.getJSONObject("geocode");
                            double latitude = geocode.getDouble("latitude");
                            double longitude = geocode.getDouble("longitude");

                            FavouritesDatabase faveDB = new FavouritesDatabase(getApplicationContext());    // adds Restaurant to Favourites
                            faveDB.insertFavourite(businessName, mRestaurant, latitude, longitude);
                            faveDB.close();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mHeader.setText("Cannot connect to server. Try again!");
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String>  headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                headers.put("x-api-version", "2");
                return headers;
            }
        };
        queue.add(jsonObjRequest);
    }

}
