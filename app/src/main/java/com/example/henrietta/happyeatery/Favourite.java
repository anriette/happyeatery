package com.example.henrietta.happyeatery;

public class Favourite {

    private int id;
    private String name;
    private int FSAid;
    private String latitude;
    private String longitude;

    public Favourite (int id, String name, int FSAid, String latitude, String longitude) {
        this.id = id;
        this.name = name;
        this.FSAid = FSAid;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getID () {
        return id;
    }

    public String getName () {
        return name;
    }

    public int getFSAid () {
        return FSAid;
    }

    public String getLatitude () {
        return latitude;
    }

    public String getLongitude () {
        return longitude;
    }

    //edits output to favourites list
    @Override
    public String toString() {  // edits the output when the toString() is called (in the ArrayAdapter)
        return getName() + " \t(" + getFSAid() + ")";
    }

}
