package com.example.henrietta.happyeatery;

import android.app.SearchManager;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class FavouriteRestaurant extends FragmentActivity implements OnMapReadyCallback, DatabaseHandler {

    TextView mHeader;
    TextView mInformation;
    Switch mFavourite;
    Button mLookUp;
    Button mDirections;
    int mRestaurant;// holds FSAid of selected restaurant
    String mCity;

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.map_restaurant_portrait);

        mHeader = findViewById(R.id.uiRestaurantHeaderTextView);
        mInformation = findViewById(R.id.uiRestaurantInfoTextTextView);
        mFavourite = findViewById(R.id.uiFavouriteSwitch);
        mLookUp = findViewById(R.id.uiLookUpButton);
        mDirections = findViewById(R.id.uiDirectionsButton);

        Intent intent = getIntent();
        mRestaurant = intent.getIntExtra("ID", 0);

        mFavourite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {    // updates whether the Restaurant is in the Favourites database, based on switch status
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked == true)
                    addToFavourites();
                else
                    removeFromFavourites();
            }
        });

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.uiResultsMapFragment);
        mapFragment.getMapAsync(this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        setSharedPreferences();
        mRestaurant = 0;
    }

    private void setSharedPreferences() {   //stores current restaurant in case of onPause/onStop
        SharedPreferences favouritePreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor favouriteEditor = favouritePreferences.edit();
        favouriteEditor.putInt("FSAid", mRestaurant);
        favouriteEditor.apply();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mRestaurant == 0) { //if onCreate does not provide an id, get one from SharedPreferences
            SharedPreferences favouritePreferences = PreferenceManager.getDefaultSharedPreferences(this);
            mRestaurant = favouritePreferences.getInt("FSAid", 0);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng resultPosition = getLocation();

        if (resultPosition != null) {
            mMap.addMarker(new MarkerOptions().position(resultPosition).icon(BitmapDescriptorFactory.defaultMarker(0)).title("Your selection"));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(resultPosition, 17));

            updateInfo();   // gets info on Restaurant and displays it
            getDBstatus();  // adjusts switch according to DB status
        }
    }

    private LatLng getLocation() {

        FavouritesDatabase faveDB = new FavouritesDatabase(this);
        LatLng restaurantLocation = faveDB.getLocation(mRestaurant);
        faveDB.close();

        return restaurantLocation;
    }

    private void updateInfo() {
        String requestUrl = "http://api.ratings.food.gov.uk/Establishments?" +
                "id=" + mRestaurant +
                "&pageSize=50&pageNumber=1";

        RequestQueue queue = Volley.newRequestQueue(this);

        JsonObjectRequest jsonObjRequest = new JsonObjectRequest(Request.Method.GET, requestUrl , null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String businessName = response.getString("BusinessName");
                            int rating = response.getInt("RatingValue");
                            JSONObject scores = response.getJSONObject("scores");
                            int hygiene = calculateDisplayRating(scores.getInt("Hygiene"));
                            int cleanliness = calculateDisplayRating(scores.getInt("Structural"));
                            int management = calculateDisplayRating(scores.getInt("ConfidenceInManagement"));
                            String inspectionDate = response.getString("RatingDate");
                            String businessType = response.getString("BusinessType");
                            String postCode = response.getString("PostCode");
                            String address = response.getString("AddressLine1");

                            //displays data
                            String date = inspectionDate.substring(0,10);   //only prints date, not time
                            mInformation.setText(rating + " / 5\n" +
                                        hygiene + "\n" +
                                        cleanliness + "\n" +
                                        management + "\n" +
                                        date + "\n" +
                                        businessType + "\n" +
                                        address + " " + postCode);

                            //sets info for buttons
                            String[] addressParts = address.split(" ");     // divides address field into several parts
                            String lastPart = addressParts[addressParts.length-1];  // extracts city
                            mCity = lastPart;

                            //updates GUI
                            mHeader.setText(businessName);
                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                            mHeader.setText("No information available");
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mHeader.setText("Cannot connect to server. Try again!");
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                headers.put("x-api-version", "2");
                return headers;
            }
        };
        queue.add(jsonObjRequest);
    }

    public int calculateDisplayRating(int DBRating) {   //changes displayed number to one that makes sense for the user
        int displayResult;

        if (DBRating == 0)
            displayResult = 5;
        else if (DBRating == 5)
            displayResult = 4;
        else if (DBRating == 10)
            displayResult = 3;
        else if (DBRating == 15)
            displayResult = 2;
        else if (DBRating == 20)
            displayResult = 1;
        else
            displayResult = 0;

        return displayResult;
    }

    public void getDBstatus() { // checks status in DB and sets switch
        FavouritesDatabase faveDB = new FavouritesDatabase(getApplicationContext());
        Favourite check = faveDB.getFavourite(mRestaurant);
        if (check != null) {
            mFavourite.setChecked(true);
        }
        else {
            mFavourite.setChecked(false);
        }
        faveDB.close();
    }

    //onClick for look up button - to look up restaurant opening hours online
    public void lookUpOnline(View view) {
        String restaurant = mHeader.getText().toString();

        Intent openingHoursIntent = new Intent (Intent.ACTION_WEB_SEARCH);
        openingHoursIntent.putExtra(SearchManager.QUERY, restaurant + " " + mCity + " opening hours");
        startActivity(openingHoursIntent);
    }

    //onClick for share button - to share restaurant via app chosen by user
    public void shareRestaurant(View view) {
        String restaurant = mHeader.getText().toString();

        Intent shareIntent = new Intent (Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, "Check out " + restaurant + " in " + mCity + "! I think you might like it.");
        startActivity(shareIntent);
    }

    public void addToFavourites() { //checks if restaurant is in faveDB, adds if it is not.
        FavouritesDatabase faveDB = new FavouritesDatabase(this);
        Favourite check = faveDB.getFavourite(mRestaurant);

        if(check == null) {
            addToDatabase();
        }

        faveDB.close();
    }

    public void removeFromFavourites() { //checks if restaurant is in faveDB, removes if it is.
        FavouritesDatabase faveDB = new FavouritesDatabase(this);
        Favourite check = faveDB.getFavourite(mRestaurant);
        if(check != null)
            faveDB.deleteFavourite(mRestaurant);
        faveDB.close();
    }

    public void addToDatabase() {
        String requestUrl = "http://api.ratings.food.gov.uk/Establishments?" +
                "id=" + mRestaurant +
                "&pageSize=50&pageNumber=1";

        RequestQueue queue = Volley.newRequestQueue(this);

        JsonObjectRequest jsonObjRequest = new JsonObjectRequest(Request.Method.GET, requestUrl , null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            //Info for database
                            String businessName = response.getString("BusinessName");
                            JSONObject geocode = response.getJSONObject("geocode");
                            double latitude = geocode.getDouble("latitude");
                            double longitude = geocode.getDouble("longitude");

                            FavouritesDatabase faveDB = new FavouritesDatabase(getApplicationContext());    // adds Restaurant to Favourites db
                            faveDB.insertFavourite(businessName, mRestaurant, latitude, longitude);
                            faveDB.close();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mHeader.setText("Cannot connect to server. Try again!");
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String>  headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                headers.put("x-api-version", "2");
                return headers;
            }
        };
        queue.add(jsonObjRequest);
    }

}
