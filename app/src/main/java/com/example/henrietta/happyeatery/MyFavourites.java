package com.example.henrietta.happyeatery;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MyFavourites extends Activity {

    ListView mListOfFavourites;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_favourites);
    }

    @Override
    protected void onResume() { // creates the list onResume, to make sure it updates if the user removes a restaurant from the favourite database
        super.onResume();

        ArrayAdapter<Favourite> adapter = new ArrayAdapter<>(this, android.R.layout.simple_expandable_list_item_1, new FavouritesDatabase(this).getAllFavourites());
        mListOfFavourites = findViewById(R.id.uiFavouritesListView);
        mListOfFavourites.setAdapter(adapter);

        mListOfFavourites.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selectedFromFaveList = (mListOfFavourites.getItemAtPosition(position).toString());

                int FSAid = getFSAid(selectedFromFaveList);

                Intent intent = new Intent(getApplicationContext(), FavouriteRestaurant.class); // opens FavouriteRestaurant activity
                intent.putExtra("ID", FSAid);
                startActivity(intent);
            }
        });
    }

    private int getFSAid(String faveFromList) { //gets id from selected item in the list

        String removeName = faveFromList.substring(faveFromList.lastIndexOf("(") + 1);
        String removeLastChar = removeName.substring(0, (removeName.length()-1));
        int FSAid = Integer.parseInt(removeLastChar);

        return FSAid;
    }

}
