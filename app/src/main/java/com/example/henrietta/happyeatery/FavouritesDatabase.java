package com.example.henrietta.happyeatery;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

public class FavouritesDatabase extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "favouritesDB";
    public static final int DATABASE_VERSION = 1;
    public static final String TABLE_NAME = "FAVOURITES";
    public static final String COLUMN_NAME0 = "_id";
    public static final String COLUMN_NAME1 = "name";
    public static final String COLUMN_NAME2 = "FSAid";
    public static final String COLUMN_NAME3 = "latitude";
    public static final String COLUMN_NAME4 = "longitude";

    public FavouritesDatabase (Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String sql = "CREATE TABLE " + TABLE_NAME + "(" +
                COLUMN_NAME0 + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_NAME1 + " TEXT, " +
                COLUMN_NAME2 + " INTEGER, " +
                COLUMN_NAME3 + " REAL, " +
                COLUMN_NAME4 + " REAL);";

        db.execSQL(sql);
    }

    public void onUpgrade (SQLiteDatabase db, int oldVersion, int newVersion) {
        if(oldVersion < 2) {
            //updates the table to contain a new column, to hold the number of visits to an establishment, say if updated app has a checkin funktion
            final String COLUMN_NAME5 = "noOfVisits";

            final String updateToSecondVersion = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + COLUMN_NAME5 + " int;";
            db.execSQL(updateToSecondVersion);
        }
    }

    public boolean insertFavourite(String name, int FSAid, double latitude, double longitude) {
        SQLiteDatabase faveDB = getWritableDatabase();

        ContentValues favouriteInfo = getFavouriteInfo(name, FSAid, latitude, longitude);
        faveDB.insert(TABLE_NAME, null, favouriteInfo);
        return true;

    }

    public void deleteFavourite (int ID) {
        SQLiteDatabase faveDB = getWritableDatabase();
        faveDB.delete(TABLE_NAME,"FSAid = ?", new String[]{ Integer.toString(ID)});
    }

    public Favourite getFavourite (int ID) {
        SQLiteDatabase faveDB = getReadableDatabase();
        String test = "" + ID;
        Favourite favourite = null;
        Cursor cursor = faveDB.query(TABLE_NAME, new String[] {COLUMN_NAME0, COLUMN_NAME1, COLUMN_NAME2, COLUMN_NAME3, COLUMN_NAME4},
                                     "FSAid = ?", new String[] {test}, null, null, null );  // querying on FSAid to make sure only one result is returned
        if(cursor != null) {
            if(cursor.moveToFirst()) {
                favourite = new Favourite(Integer.parseInt(cursor.getString(0)),
                                        cursor.getString(1),
                                        Integer.parseInt(cursor.getString(2)),
                                        cursor.getString(3),
                                        cursor.getString(4));
            }
            cursor.close();
            faveDB.close();
        }

        return favourite;
    }

    public ArrayList<Favourite> getAllFavourites () {
        ArrayList<Favourite> favourites = new ArrayList<>();

        SQLiteDatabase faveDB = getReadableDatabase();
        Cursor cursor = faveDB.query(TABLE_NAME, new String[] {COLUMN_NAME0, COLUMN_NAME1, COLUMN_NAME2, COLUMN_NAME3, COLUMN_NAME4},
                                     null, null, null, null, null );
        if(cursor!= null) {
            if(cursor.moveToFirst()) {
                do {
                    favourites.add(new Favourite(Integer.parseInt(cursor.getString(0)),
                            cursor.getString(1),
                            Integer.parseInt(cursor.getString(2)),
                            cursor.getString(3),
                            cursor.getString(4)));
                } while(cursor.moveToNext());
            }
            cursor.close();
            faveDB.close();
        }

        return favourites;
    }

    public int getNoOfFavourites() {
        SQLiteDatabase faveDB = getReadableDatabase();

        Cursor cursor = faveDB.query(TABLE_NAME, new String[] {COLUMN_NAME0}, null, null, null, null, null);
        cursor.moveToFirst();

        int noOfFavourites = cursor.getCount();
        cursor.close();
        faveDB.close();

        return noOfFavourites;
    }

    public LatLng getLocation (int ID) {
        SQLiteDatabase faveDB = getReadableDatabase();
        String FSAid = "" + ID;
        Cursor cursor = faveDB.query(TABLE_NAME, new String[] {COLUMN_NAME3, COLUMN_NAME4},
                "FSAid = ?", new String[] {FSAid}, null, null, null );

        double restaurantLatitude = 0;
        double restaurantLongitude = 0;
        if(cursor != null) {
            if(cursor.moveToFirst()) {
                restaurantLatitude = Double.parseDouble(cursor.getString(0));
                restaurantLongitude = Double.parseDouble(cursor.getString(1));
            }
            cursor.close();
            faveDB.close();
        }

        LatLng restaurantLocation = new LatLng(restaurantLatitude, restaurantLongitude);
        return restaurantLocation;

    }

    private ContentValues getFavouriteInfo (String name, int FSAid, double latitude, double longitude) {
        ContentValues faveInfo = new ContentValues();

        faveInfo.put(COLUMN_NAME1, name);
        faveInfo.put(COLUMN_NAME2, FSAid);
        faveInfo.put(COLUMN_NAME3, latitude);
        faveInfo.put(COLUMN_NAME4, longitude);

        return faveInfo;
    }

}
