package com.example.henrietta.happyeatery;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.io.IOException;
import java.util.List;

public class MainActivity extends Activity implements AdapterView.OnItemSelectedListener {

    EditText mLocation;
    Location mDeviceLocation;
    Button mMyLocation;
    private static final String MY_LOCATION = "My location";

    Spinner mDistanceSpinner;
    ArrayAdapter mSpinnerAdapter;
    int mSelectedDistance;

    RadioGroup mRating;
    int mSelectedRating;

    private FusedLocationProviderClient mFusedLocationClient;
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mLocation = findViewById(R.id.uiLocationEditText);
        mMyLocation = findViewById(R.id.uiLocationButton);

        mSpinnerAdapter = ArrayAdapter.createFromResource(this, R.array.distanceSpinnerContents, android.R.layout.simple_spinner_item);
        mDistanceSpinner = findViewById(R.id.uiDistanceSpinner);
        mDistanceSpinner.setAdapter(mSpinnerAdapter);
        mDistanceSpinner.setOnItemSelectedListener(this);

        mRating = findViewById(R.id.uiRatingRadioGroup);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
    }

    public void onStart() {
        super.onStart();
        if (!checkPermission()) {
            requestPermission();
        }
        else {
            getLastLocation();  // if app already has permission
        }
    }

    private boolean checkPermission() {
        int permissions = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissions == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(MainActivity.this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                MY_PERMISSIONS_REQUEST_LOCATION);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_LOCATION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLastLocation();
            } else {
                mMyLocation.setVisibility(View.INVISIBLE); // My Location button is set to invisible, to that it cannot be used
            }
        }
    }

    @SuppressWarnings("MissingPermission")
    private void getLastLocation() {
        mFusedLocationClient.getLastLocation()
                .addOnCompleteListener(this, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            mDeviceLocation = task.getResult(); //device location is found, and stored in mDeviceLocation
                        }
                    }
                });
    }

    @Override
    protected void onPause() {
        super.onPause();

        setSharedPreferences();
    }

    private void setSharedPreferences() {   //stores the entered information in case of onPause/onStop
        SharedPreferences entryPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor entryEditor = entryPreferences.edit();
        entryEditor.putString("location", mLocation.getText().toString());
        entryEditor.putInt("distance", mSelectedDistance);
        entryEditor.putInt("rating", mSelectedRating);
        entryEditor.apply();
    }

    @Override
    protected void onResume() {
        super.onResume();

        Button myFavouritesButton = findViewById(R.id.uiMyFavouritesButton);
        int noOfFavourites = checkForFavourites();
        myFavouritesButton.setText("My favourite eateries (" + noOfFavourites + ")");

        getSharedPreferences();
    }

    private void getSharedPreferences() {   // gets stored information in case of onPause/onStop
        SharedPreferences entryPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        mLocation.setText(entryPreferences.getString("location", ""));

        mSelectedDistance = entryPreferences.getInt("distance", 0);
        mDistanceSpinner.setSelection(mSelectedDistance-1);

        mSelectedRating = entryPreferences.getInt("rating", 5);
        ((RadioButton)mRating.getChildAt(mSelectedRating)).setChecked(true);    //checks latest selected rating
    }

    public int checkForFavourites() {   // gets no of favourites stored in db
        FavouritesDatabase faveDB = new FavouritesDatabase(this);
        int noOfFavourites = faveDB.getNoOfFavourites();
        return noOfFavourites;
    }

    //onClick for MyLocation button - puts "My location" in the location input box
    public void setCurrentLocation (View view) {
        if (mDeviceLocation != null)
            mLocation.setText(MY_LOCATION);
        else
            Toast.makeText(this, "Please click on My Location again", Toast.LENGTH_LONG).show(); // in case the device location has yet to be determined
    }

    //onClick for Favourites button - links to MyFavourites
    public void displayFavourites (View view) {
        startActivity(new Intent(this, MyFavourites.class));
    }

    //onClick for Search button - links to Result if location, distance and rating are available
    public void displaySearchResults (View view) {
        if (mLocation.length() > 0
            && mSelectedRating > -1 && mSelectedRating <= 6
            && mRating.getCheckedRadioButtonId() > -1) {    // if all needed information is available - Result activity is started

            String enteredLocation = mLocation.getText().toString();
            LatLng selectedLocation;

            if (mDeviceLocation != null && mLocation.getText().toString().equals(MY_LOCATION)) { // makes sure device location is used if My Location button has been pressed
                selectedLocation = new LatLng(mDeviceLocation.getLatitude(), mDeviceLocation.getLongitude());
            }
            else {
                selectedLocation = checkLocation(this, enteredLocation);
            }

            if (selectedLocation != null) {
                Intent intent = new Intent (this, Result.class);
                Bundle position = new Bundle();
                position.putString("enteredLocation", enteredLocation);
                position.putParcelable("location", selectedLocation);
                position.putInt("distance", mSelectedDistance);
                position.putInt("rating", mSelectedRating);
                intent.putExtra("bundle", position);
                startActivity(intent);
            }
            else
                Toast.makeText(this, "Your location is not recognised. Please change it and try again.", Toast.LENGTH_LONG).show();
        }
        else if (mLocation.length() < 1)
            Toast.makeText(this, "Please enter a location.", Toast.LENGTH_LONG).show();
        else
            Toast.makeText(this, "Please select a rating.", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        TextView distance = (TextView) view;
        mSelectedDistance = Integer.parseInt(distance.getText().toString());
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        Toast.makeText(this, "Please select a distance", Toast.LENGTH_LONG).show();
    }

    public void checkRadioButton(View view) {   // sets rating to selected radio button
        int currentSelection = mRating.getCheckedRadioButtonId();

        RadioButton radioButton = findViewById(currentSelection);

        mSelectedRating = Integer.parseInt(radioButton.getText().toString());
    }

    public LatLng checkLocation(Context context, String enteredLocation) {  // gets position of entered starting position
        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng theLocation = null;

        try {
            address = coder.getFromLocationName(enteredLocation, 1);
            if (address == null) {  //returns null if nothing is found
                return null;
            }
            if (address.size() == 0) {
                return null;
            }

            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();

            theLocation = new LatLng(location.getLatitude(), location.getLongitude() );

        } catch (IOException ex) {

            ex.printStackTrace();
        }

        return theLocation;
    }

}
